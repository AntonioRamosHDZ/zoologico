package com.antonio.gruposalinas.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antonio.gruposalinas.app.entity.Aguila;
import com.antonio.gruposalinas.app.services.AguilaServicio;

@RestController
@RequestMapping("/api/aguila")
public class AguilaController {
	
	@Autowired
	private AguilaServicio aguilaservicio;
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Aguila aguila){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(aguilaservicio.save(aguila));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable(value="id") Long id){
		Optional<Aguila> oAguila = aguilaservicio.findById(id);
		if(!oAguila.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oAguila);
	}
	
		
		
		@PutMapping("/{id}")
		public ResponseEntity<?> update(@RequestBody Aguila aguiladetalles, @PathVariable(value="id")Long id){
			Optional<Aguila> oAguila = aguilaservicio.findById(id);
			if(!oAguila.isPresent())
			{
				return ResponseEntity.notFound().build();
			}
			
			
			oAguila.get().setColor(aguiladetalles.getColor());
			oAguila.get().setEdad(aguiladetalles.getEdad());
			oAguila.get().setTamaño(aguiladetalles.getTamaño());
			oAguila.get().setAlimentacion(aguiladetalles.getAlimentacion());
			oAguila.get().setHabitad(aguiladetalles.getHabitad());
			
			return ResponseEntity.status(HttpStatus.CREATED).body(aguilaservicio.save(oAguila.get()));
		}
		
		@DeleteMapping("/{id}")
		public ResponseEntity<?> delate(@PathVariable(value="id")Long id){
			Optional<Aguila> oAguila =aguilaservicio.findById(id);
			if(!oAguila.isPresent()) {
				return ResponseEntity.notFound().build();
			}
			aguilaservicio.deleteById(id);
			return ResponseEntity.ok(oAguila);
		}
		
		@GetMapping("/all")
		public ResponseEntity<?> readall(){
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(aguilaservicio.findAll());
		}

}
