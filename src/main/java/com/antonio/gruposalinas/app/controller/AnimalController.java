package com.antonio.gruposalinas.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antonio.gruposalinas.app.services.AnimalServicio;
import com.antonio.gruposalinas.app.entity.Animal;




@RestController
@RequestMapping("/api/animal")
public class AnimalController {
	
	@Autowired
	private AnimalServicio animalservicio;
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Animal animal){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(animalservicio.save(animal));
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable(value="id") Long id){
		Optional<Animal> oAnimal = animalservicio.findById(id);
		if (!oAnimal.isPresent())
		{
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oAnimal);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<?> update(@RequestBody Animal animaldetalles, @PathVariable(value="id")Long id){
		Optional<Animal> oAnimal =animalservicio.findById(id);
		if(!oAnimal.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		oAnimal.get().setColor(animaldetalles.getColor());
		oAnimal.get().setEspecie(animaldetalles.getEspecie());
		oAnimal.get().setEdad(animaldetalles.getEdad());
		oAnimal.get().setColor(animaldetalles.getTamaño());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(animalservicio.save(oAnimal.get()));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delate(@PathVariable(value ="id")Long id){
		Optional<Animal> oAnimal = animalservicio.findById(id);
		if(!oAnimal.isPresent()) {
			return ResponseEntity.notFound().build();
			
		}
		
		animalservicio.deleteById(id);
		return ResponseEntity.ok(oAnimal);
		
	}

	@GetMapping("/all")
	public ResponseEntity<?> readall(){
		
		 return ResponseEntity.status(HttpStatus.ACCEPTED).body(animalservicio.findAll());
		
	}

}
