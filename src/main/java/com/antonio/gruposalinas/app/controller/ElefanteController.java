package com.antonio.gruposalinas.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antonio.gruposalinas.app.entity.Elefante;
import com.antonio.gruposalinas.app.services.ElefanteServicio;

@RestController
@RequestMapping("/api/Elefante")
public class ElefanteController {
	
	@Autowired
	private ElefanteServicio elefanteservicio;
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Elefante elefante){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(elefanteservicio.save(elefante));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable(value="id") Long id){
		Optional<Elefante> oElefante = elefanteservicio.findById(id);
		if(!oElefante.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oElefante);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Elefante elefanteDetalles, @PathVariable(value ="id")Long id){
		Optional<Elefante> oElefante = elefanteservicio.findById(id);
		if(!oElefante.isPresent()) {
			return ResponseEntity.notFound().build();
			
		}
		
		oElefante.get().setColor(elefanteDetalles.getColor());
		oElefante.get().setEdad(elefanteDetalles.getEdad());
		oElefante.get().setTamaño(elefanteDetalles.getTamaño());
		oElefante.get().setAlimentacion(elefanteDetalles.getAlimentacion());
		oElefante.get().setHabitad(elefanteDetalles.getHabitad());
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(elefanteservicio.save(oElefante.get()));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delate(@PathVariable(value ="id")Long id){
		Optional<Elefante> oUsuario = elefanteservicio.findById(id);
		if(!oUsuario.isPresent()) {
			return ResponseEntity.notFound().build();
			
		}
		
		elefanteservicio.deleteById(id);
		return ResponseEntity.ok(oUsuario);
		
	}

	@GetMapping("/all")
	public ResponseEntity<?> readall(){
		
		 return ResponseEntity.status(HttpStatus.ACCEPTED).body(elefanteservicio.findAll());
		
	}

}