package com.antonio.gruposalinas.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.antonio.gruposalinas.app.entity.Leon;
import com.antonio.gruposalinas.app.services.LeonServicio;

@RestController
@RequestMapping("/api/Leon")
public class LeonController {
	
	@Autowired
	private LeonServicio leonservicio;
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Leon leon){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(leonservicio.save(leon));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable(value="id") Long id){
		Optional<Leon> oLeon = leonservicio.findById(id);
		if(!oLeon.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oLeon);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Leon leondetalles, @PathVariable(value="id")Long id){
		Optional<Leon> oLeon = leonservicio.findById(id);
		if(!oLeon.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		oLeon.get().setColor(leondetalles.getColor());
		oLeon.get().setEdad(leondetalles.getEdad());
		oLeon.get().setTamaño(leondetalles.getTamaño());
		oLeon.get().setAlimentacion(leondetalles.getAlimentacion());
		oLeon.get().setHabitad(leondetalles.getHabitad());
		return ResponseEntity.status(HttpStatus.CREATED).body(leonservicio.save(oLeon.get()));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delate(@PathVariable(value="id")Long id){
		Optional<Leon> oAguila =leonservicio.findById(id);
		if(!oAguila.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		leonservicio.deleteById(id);
		return ResponseEntity.ok(oAguila);
	}
	
	@GetMapping("/all")
	public ResponseEntity<?> readall(){
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(leonservicio.findAll());
	}

}