package com.antonio.gruposalinas.app.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Aguila")
public class Aguila implements Serializable{
	private static final long serialVersionUID =1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;
	@Column(name="color", nullable = false )
	private String color;
	@Column(name="edad", nullable = false )
	private int edad;
	@Column(name="tamaño", nullable = false )
	private String tamaño;
	@Column(name="Alimento", nullable = false )
	private String Alimentacion;
	@Column(name="habitad", nullable = false )
	private String habitad;
	
	
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getTamaño() {
		return tamaño;
	}
	public void setTamaño(String tamaño) {
		this.tamaño = tamaño;
	}
	public String getAlimentacion() {
		return Alimentacion;
	}
	public void setAlimentacion(String alimentacion) {
		Alimentacion = alimentacion;
	}
	public String getHabitad() {
		return habitad;
	}
	public void setHabitad(String habitad) {
		this.habitad = habitad;
	}
	
	
}