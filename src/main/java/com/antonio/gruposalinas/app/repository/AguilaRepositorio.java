package com.antonio.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.antonio.gruposalinas.app.entity.Aguila;



@Repository
public interface AguilaRepositorio extends JpaRepository<Aguila, Long>{

}
