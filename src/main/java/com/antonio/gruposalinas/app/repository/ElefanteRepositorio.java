package com.antonio.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.antonio.gruposalinas.app.entity.Elefante;

@Repository
public interface ElefanteRepositorio extends JpaRepository<Elefante, Long>{

}
