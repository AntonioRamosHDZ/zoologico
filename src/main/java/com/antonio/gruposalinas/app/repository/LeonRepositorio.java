package com.antonio.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.antonio.gruposalinas.app.entity.Leon;

@Repository
public interface LeonRepositorio extends JpaRepository<Leon, Long>{

}
