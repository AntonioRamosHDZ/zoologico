package com.antonio.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.antonio.gruposalinas.app.entity.Aguila;

@Service
public interface AguilaServicio {
	
	public Iterable<Aguila>findAll();
	public Page<Aguila>findAll(Pageable pageable);
	public Optional<Aguila>findById(Long id);
	public Aguila save(Aguila aguila);
	public void deleteById(Long id);

}
