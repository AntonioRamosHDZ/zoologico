package com.antonio.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.antonio.gruposalinas.app.entity.Aguila;
import com.antonio.gruposalinas.app.repository.AguilaRepositorio;

@Service
public class AguilaServiciosImplements implements AguilaServicio{

	@Autowired
	private AguilaRepositorio aguilarepositorio;
	
	
	@Override
	@Transactional(readOnly=true)
	public Iterable<Aguila> findAll() {
		// TODO Auto-generated method stub
		return aguilarepositorio.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Page<Aguila> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return aguilarepositorio.findAll(pageable);
	}

	@Override
	@Transactional(readOnly=true)
	public Optional<Aguila> findById(Long id) {
		// TODO Auto-generated method stub
		return aguilarepositorio.findById(id);
	}

	@Override
	@Transactional
	public Aguila save(Aguila aguila) {
		// TODO Auto-generated method stub
		return aguilarepositorio.save(aguila);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		aguilarepositorio.deleteById(id);
		
	}
	

}
