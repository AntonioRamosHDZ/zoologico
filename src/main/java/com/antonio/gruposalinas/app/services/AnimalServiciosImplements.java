package com.antonio.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.antonio.gruposalinas.app.entity.Animal;
import com.antonio.gruposalinas.app.repository.AnimalRepositorio;

@Service
public class AnimalServiciosImplements implements AnimalServicio{

	@Autowired
	private AnimalRepositorio animalrepositorio;
	
	
	@Override
	@Transactional(readOnly=true)
	public Iterable<Animal> findAll() {
		// TODO Auto-generated method stub
		return animalrepositorio.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Page<Animal> findAll(Pageable pageble) {
		// TODO Auto-generated method stub
		return animalrepositorio.findAll(pageble);
	}

	@Override
	@Transactional(readOnly=true)
	public Optional<Animal> findById(Long id) {
		// TODO Auto-generated method stub
		return animalrepositorio.findById(id);
	}

	@Override
	@Transactional
	public Animal save(Animal animal) {
		// TODO Auto-generated method stub
		return animalrepositorio.save(animal);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		
		animalrepositorio.deleteById(id);
		
	}

}
