package com.antonio.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.antonio.gruposalinas.app.entity.Elefante;

@Service
public interface ElefanteServicio{
	public Iterable<Elefante>findAll();
	public Page<Elefante>findAll(Pageable pageable);
	public Optional<Elefante>findById(Long id);
	public Elefante save (Elefante elefante);
	public void deleteById(Long id);

}
