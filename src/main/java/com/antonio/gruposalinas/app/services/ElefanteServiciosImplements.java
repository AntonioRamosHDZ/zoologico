package com.antonio.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.antonio.gruposalinas.app.entity.Elefante;
import com.antonio.gruposalinas.app.repository.ElefanteRepositorio;

@Service
public class ElefanteServiciosImplements implements ElefanteServicio{

	
	@Autowired
	private ElefanteRepositorio elefanterepositorio;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Elefante> findAll() {
		// TODO Auto-generated method stub
		return elefanterepositorio.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Elefante> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return elefanterepositorio.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Elefante> findById(Long id) {
		// TODO Auto-generated method stub
		return elefanterepositorio.findById(id);
	}

	@Override
	@Transactional
	public Elefante save(Elefante elefante) {
		// TODO Auto-generated method stub
		return elefanterepositorio.save(elefante);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		elefanterepositorio.deleteById(id);
		
	}
	
	

}
