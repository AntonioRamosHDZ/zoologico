package com.antonio.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.antonio.gruposalinas.app.entity.Leon;
import com.antonio.gruposalinas.app.repository.LeonRepositorio;

@Service

public class LeonServiciosImplements implements LeonServicio{

	@Autowired
	private LeonRepositorio leonrepositorio;
	
	@Override
	@Transactional(readOnly=true)
	public Iterable<Leon> findAll() {
		// TODO Auto-generated method stub
		return leonrepositorio.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Page<Leon> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return leonrepositorio.findAll(pageable);
	}

	@Override
	@Transactional(readOnly=true)
	public Optional<Leon> findById(Long id) {
		// TODO Auto-generated method stub
		return leonrepositorio.findById(id);
	}

	@Override
	@Transactional
	public Leon save(Leon leon) {
		// TODO Auto-generated method stub
		return leonrepositorio.save(leon);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		leonrepositorio.deleteById(id);
	}
	

}
